package com.ssd.ssdedit.tool

import com.ssd.ssdedit.context.Configurable
import com.ssd.ssdedit.context.Context

interface ToolContext<T> : Context, Configurable {
    val properties: T;
}
