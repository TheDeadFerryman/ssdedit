package com.ssd.ssdedit.tool

import com.ssd.ssdedit.canvas.CanvasApplier
import com.ssd.ssdedit.properties.PropertiesDrawerContext
import com.ssd.ssdedit.properties.PropertiesDrawerFactory
import com.ssd.ssdedit.toolbar.ToolbarDrawable

interface ToolFactory<T : ToolContext<*>> {
    fun createContext(): T

    fun createToolbarDrawable(): ToolbarDrawable

    fun createPropertiesDrawer(): PropertiesDrawerFactory

    fun createCanvasApplier(): CanvasApplier<T>
}