package com.ssd.ssdedit.toolbar

interface ToolbarContext {
    fun append(drawable: ToolbarDrawable)
}