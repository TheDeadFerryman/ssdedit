package com.ssd.ssdedit.toolbar

interface ToolbarDrawable {
    fun attach(context: ToolbarContext)
}