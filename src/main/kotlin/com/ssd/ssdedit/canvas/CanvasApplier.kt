package com.ssd.ssdedit.canvas

import com.ssd.ssdedit.context.Context
import com.ssd.ssdedit.tool.ToolContext

interface CanvasApplier<T : ToolContext<*>> {
    fun execute(canvas: Canvas, toolContext: T)
}
