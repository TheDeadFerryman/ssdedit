package com.ssd.ssdedit.event

import com.ssd.ssdedit.context.Context

typealias EventHandler<E, C> = (event: E, context: C) -> Unit

interface EventController<E : Event, C : Context> : EventBus {
    fun bindContext(context: C)

    fun bindEvent(
        eventType: String,
        handler: EventHandler<in E, in C>
    ): Int

    fun unbindEvent(eventType: String, handlerId: Int)
}