package com.ssd.ssdedit.event

interface EventBus {
    fun send(event: Event)
}