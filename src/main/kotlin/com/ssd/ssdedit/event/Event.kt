package com.ssd.ssdedit.event

interface Event {
    val type: String;
}