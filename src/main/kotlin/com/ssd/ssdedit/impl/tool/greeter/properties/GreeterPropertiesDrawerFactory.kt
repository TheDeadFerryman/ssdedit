package com.ssd.ssdedit.impl.tool.greeter.properties

import com.ssd.ssdedit.context.Context
import com.ssd.ssdedit.event.Event
import com.ssd.ssdedit.event.EventController
import com.ssd.ssdedit.impl.tool.greeter.GreeterToolContext
import com.ssd.ssdedit.impl.tool.greeter.properties.events.GreeterPropertiesEvents
import com.ssd.ssdedit.properties.PropertiesDrawer
import com.ssd.ssdedit.properties.PropertiesDrawerContext
import com.ssd.ssdedit.properties.PropertiesDrawerFactory

class GreeterPropertiesDrawerFactory : PropertiesDrawerFactory  {
    override fun createContext(): PropertiesDrawerContext {
        return GreeterPropertiesDrawerContext()
    }

    override fun createDrawer(): PropertiesDrawer {
        TODO("Not yet implemented")
    }

    override fun createController(): EventController<Event, Context> {
        TODO("Not yet implemented")
    }
}