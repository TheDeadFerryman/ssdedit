package com.ssd.ssdedit.impl.tool.greeter

import com.ssd.ssdedit.tool.ToolContext

class GreeterToolContext(
    override var properties: Properties
) : ToolContext<GreeterToolContext.Properties> {
    data class Properties(
        var name: String,
    )
}