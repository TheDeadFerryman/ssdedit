package com.ssd.ssdedit.impl.tool.greeter.properties

import com.ssd.ssdedit.context.Configurable
import com.ssd.ssdedit.context.Context
import com.ssd.ssdedit.impl.tool.greeter.GreeterToolContext
import com.ssd.ssdedit.properties.PropertiesDrawerContext
import com.ssd.ssdedit.window.WindowDrawerContext

class GreeterPropertiesDrawerContext : PropertiesDrawerContext {
    lateinit var context: Configurable

    override fun bind(target: Configurable) {
        context = target
    }

    override fun attach(context: WindowDrawerContext) {
        // This is a stub
    }
}