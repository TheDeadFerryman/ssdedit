package com.ssd.ssdedit.impl.tool.greeter

import com.ssd.ssdedit.canvas.Canvas
import com.ssd.ssdedit.canvas.CanvasApplier

class GreeterCanvasApplier : CanvasApplier<GreeterToolContext> {
    override fun execute(canvas: Canvas, toolContext: GreeterToolContext) {
        println("Hello, ${toolContext.properties.name}")
    }
}