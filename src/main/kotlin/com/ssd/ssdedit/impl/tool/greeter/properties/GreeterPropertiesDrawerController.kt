package com.ssd.ssdedit.impl.tool.greeter.properties

import com.ssd.ssdedit.event.Event
import com.ssd.ssdedit.event.EventController
import com.ssd.ssdedit.event.EventHandler
import com.ssd.ssdedit.impl.tool.greeter.properties.events.GreeterPropertiesEvents

typealias GreeterEventHandler = EventHandler<GreeterPropertiesEvents, GreeterPropertiesDrawerContext>

class GreeterPropertiesDrawerController : EventController<GreeterPropertiesEvents, GreeterPropertiesDrawerContext> {
    private lateinit var context: GreeterPropertiesDrawerContext
    private val handlers = mutableMapOf<String, MutableList<GreeterEventHandler>>()

    override fun bindContext(context: GreeterPropertiesDrawerContext) {
        this.context = context
    }

    override fun bindEvent(eventType: String, handler: GreeterEventHandler): Int {
        return if (handlers[eventType] != null) {
            handlers[eventType]!!.add(handler)

            handlers[eventType]!!.size - 1
        } else {
            handlers[eventType] = mutableListOf(handler)

            0
        }
    }

    override fun unbindEvent(eventType: String, handlerId: Int) {
        if ((handlers[eventType]?.size ?: 0) > handlerId) {
            handlers[eventType]?.removeAt(handlerId)
        }
    }

    override fun send(event: Event) {
        if (event is GreeterPropertiesEvents) {
            handlers[event.type]?.forEach { it.invoke(event, context) }
        }
    }
}