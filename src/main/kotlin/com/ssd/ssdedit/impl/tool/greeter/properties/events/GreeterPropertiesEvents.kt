package com.ssd.ssdedit.impl.tool.greeter.properties.events

import com.ssd.ssdedit.event.Event

sealed class GreeterPropertiesEvents : Event

data class GreeterPropertiesSetName(
    val newName: String,
) : GreeterPropertiesEvents() {
    override val type = "GreeterPropertiesSetName"
}