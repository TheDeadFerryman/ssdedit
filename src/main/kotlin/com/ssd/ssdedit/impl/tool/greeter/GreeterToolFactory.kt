package com.ssd.ssdedit.impl.tool.greeter

import com.ssd.ssdedit.canvas.CanvasApplier
import com.ssd.ssdedit.impl.tool.greeter.properties.GreeterPropertiesDrawerFactory
import com.ssd.ssdedit.properties.PropertiesDrawerContext
import com.ssd.ssdedit.properties.PropertiesDrawerFactory
import com.ssd.ssdedit.tool.ToolFactory
import com.ssd.ssdedit.toolbar.ToolbarDrawable

class GreeterToolFactory : ToolFactory<GreeterToolContext> {
    private val INITIAL_PROPS = GreeterToolContext.Properties(name = "World")

    override fun createContext(): GreeterToolContext {
        return GreeterToolContext(INITIAL_PROPS)
    }

    override fun createToolbarDrawable(): ToolbarDrawable {
        return GreeterToolbarDrawable();
    }

    override fun createPropertiesDrawer(): PropertiesDrawerFactory {
        return GreeterPropertiesDrawerFactory()
    }

    override fun createCanvasApplier(): CanvasApplier<GreeterToolContext> {
        return GreeterCanvasApplier()
    }
}