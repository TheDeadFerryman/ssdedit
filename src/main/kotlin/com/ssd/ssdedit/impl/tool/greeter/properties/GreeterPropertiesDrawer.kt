package com.ssd.ssdedit.impl.tool.greeter.properties

import com.ssd.ssdedit.event.EventBus
import com.ssd.ssdedit.properties.PropertiesDrawer
import com.ssd.ssdedit.window.WindowDrawerContext

class GreeterPropertiesDrawer : PropertiesDrawer {
    lateinit var bus: EventBus

    override fun bind(bus: EventBus) {
        this.bus = bus
    }

    override fun draw(context: WindowDrawerContext) {
        println("Drawing properties window")
    }
}