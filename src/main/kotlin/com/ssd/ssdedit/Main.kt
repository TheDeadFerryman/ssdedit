package com.ssd.ssdedit

import com.ssd.ssdedit.impl.canvas.StubCanvas
import com.ssd.ssdedit.impl.tool.greeter.GreeterToolFactory


fun main() {
    val toolFactory = GreeterToolFactory()

    val ctx = toolFactory.createContext()

    ctx.properties.name = "Karl"

    val propDrawer = toolFactory.createPropertiesDrawer()

    val applier = toolFactory.createCanvasApplier()

    applier.execute(StubCanvas(), ctx);
}