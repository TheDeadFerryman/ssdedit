package com.ssd.ssdedit.properties

import com.ssd.ssdedit.context.Configurable
import com.ssd.ssdedit.context.Context
import com.ssd.ssdedit.window.WindowDrawerContext

interface PropertiesDrawerContext : Context {
    fun bind(target: Configurable)

    fun attach(context: WindowDrawerContext)
}