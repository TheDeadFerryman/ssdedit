package com.ssd.ssdedit.properties

import com.ssd.ssdedit.context.Context
import com.ssd.ssdedit.event.Event
import com.ssd.ssdedit.event.EventBus
import com.ssd.ssdedit.event.EventController
import com.ssd.ssdedit.window.WindowDrawerContext

interface PropertiesDrawer {
    fun bind(bus: EventBus)

    fun draw(context: WindowDrawerContext)
}
