package com.ssd.ssdedit.properties

import com.ssd.ssdedit.context.Context
import com.ssd.ssdedit.event.Event
import com.ssd.ssdedit.event.EventController

interface PropertiesDrawerFactory {
    fun createContext(): PropertiesDrawerContext

    fun createDrawer(): PropertiesDrawer

    fun createController(): EventController<Event, Context>
}